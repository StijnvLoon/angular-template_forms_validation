import { Component } from '@angular/core';

import { User }    from '../user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  genderArray = ['Man', 'Vrouw', 'Anders']
  userArray = [];
  model = new User(0, '', '', '', 'man')

  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.submitToArray();
  }

  submitToArray() {
    this.userArray.push(this.model);
    this.model = new User(this.userArray.length, '', '', '', 'man');
    console.log(this.userArray);
  }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }
}